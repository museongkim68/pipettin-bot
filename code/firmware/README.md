# Miscelaneous firmware and scripts

## Robofuge: A centrifuge for lab automation

Sketch: [simplefoc-centrifuge](./simplefoc-centrifuge)

## ArduinoJSON-RPC example

Example code for adding RPC-like communications to an Arduino through a serial interface.

Sketch: [arduino-json_rpc_example](./arduino-json_rpc_example)

## Load-cell sensor with HX711 and the Pi Pico

Sketch: [hx711_pico](./hx711_pico)

MicroPython program for the Raspberry Pi Pico.

Reads data from an HX711 DAC, connected to a load cell sensor.

Receives simple commands through the USB serial interface.

## JSON parsing example

Sketch: [JsonParserExample](./JsonParserExample)

Reads JSON input (only `{"timestamp":1234,"value":687}` for now) from the SerialUSB interface (swap for `Serial` if no using an Arduino Zero MCU), and discards newline and carriage-return characters.
