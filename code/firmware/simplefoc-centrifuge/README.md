# SimpleFOC sketch for the Robofuge project

Sketch: [simplefoc-centrifuge.ino](./simplefoc-centrifuge.ino)

License details: [LICENSES.md](../../../LICENSES.md) (GNU Affero GPL v3).

Rotor and swing rings designs: [bldc_centrifuge](../../../models/labware/bldc_centrifuge)

Components:

- Motor: HDD BLDC.
- Motor driver: SimpleFOC mini.
- MCU: ESP32.
- Sensors: opto-endstop for homing and RPM counting and verification.

> ![parts.png](./images/parts.png)
>
> Parts of the robofuge project, a derivative of OLA's Tupperfuge portable field centrifuge (https://docs.openlabautomata.xyz/en/Mini-Docs/labware/Centrifuge).
