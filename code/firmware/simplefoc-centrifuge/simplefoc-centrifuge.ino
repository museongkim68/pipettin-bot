// Open loop motor control example
#include <SimpleFOC.h>

// Endstop stuff: https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
// const byte interruptPin = 14;
const byte interruptPin = 2; // Wemos D1 mini pin "D4" / "GPIO2"
volatile byte state = LOW;
int counter = 0;
float counter_timestamp = millis();
float elapsed = 0;
float report_interval = 2000;  // ms
//void toggle() {
//  state = !state;
//}
unsigned long currentTime = 0;       // the last time the output pin was toggled
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 4;     // the debounce time; increase if the output flickers
void IRAM_ATTR toggle() {
  // Interrupts in ESP32: https://lastminuteengineers.com/handling-esp32-gpio-interrupts-tutorial/
  // Debouncing: https://docs.arduino.cc/built-in-examples/digital/Debounce
  currentTime = millis();
  if (currentTime - lastDebounceTime >= debounceDelay) {
    lastDebounceTime = currentTime;
    state = HIGH;
    counter++;
  }
}

// BLDC motor & driver instance
BLDCMotor motor = BLDCMotor(4);  // Pole pairs HDD
//BLDCMotor motor = BLDCMotor(7);  // Pole pairs Drone

// Reference:
// BLDCDriver3PWM driver = BLDCDriver3PWM(pwmA, pwmB, pwmC, Enable(optional));
// Boards:
// BLDCDriver3PWM driver = BLDCDriver3PWM(23, 22, 1, 3);  // ESP32 WROOM (old)
// BLDCDriver3PWM driver = BLDCDriver3PWM(17, 16, 4, 0);  // ESP32 WROOM (new)
// BLDCDriver3PWM driver = BLDCDriver3PWM(11, 10, 9, 8);  // Arduino UNO
// Lolin Wemos D1 Mini: D7(GPIO13), D6(GPIO12), D5(GPIO14), D0/GPIO16
BLDCDriver3PWM driver = BLDCDriver3PWM(13, 12, 14, 16);

// Stepper motor & driver instance
//StepperMotor motor = StepperMotor(50);
//StepperDriver4PWM driver = StepperDriver4PWM(9, 5, 10, 6,  8);

//target variable
float angular_velocity = 0.0;
float current_rpm = 0.0;
float ramp_increment = 0.2;
float ramp_increment_interval = 1e4;  // ms
float direction = 1.0;
float rpm_limit = 60.0;  // 2900.0;  // At 2940-2960 rpm and 3V the motor will stall.
float angular_velocity_limit = (rpm_limit / 60) * (2 * 3.14159265359);
// float angular_velocity_limit = 2.0*3.14159265359;

// timestamp for changing direction
float timestamp_us = _micros();

// timestamp for serial messages
float timestamp_ms = millis();

// instantiate the commander
Commander command = Commander(Serial);
void doTarget(char* cmd) {
  command.scalar(&rpm_limit, cmd);
}
void doLimit(char* cmd) {
  command.scalar(&motor.voltage_limit, cmd);
}

void setup() {

  // Endstop stuff: https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), toggle, FALLING);
  // attachInterrupt(digitalPinToInterrupt(interruptPin), toggle, RISING);

  // Set built-in LED pin mode: https://circuits4you.com/2018/02/02/esp32-led-blink-example/
  // pinMode(LED_BUILTIN, OUTPUT);

  // driver config
  // power supply voltage [V]
  driver.voltage_power_supply = 12;
  // limit the maximal dc voltage the driver can set
  // as a protection measure for the low-resistance motors
  // this value is fixed on startup
  driver.voltage_limit = 10;
  driver.init();
  // link the motor and the driver
  motor.linkDriver(&driver);

  // limiting motor movements
  // limit the voltage to be set to the motor
  // start very low for high resistance motors
  // current = voltage / resistance, so try to be well under 1Amp

  // Openloop velocity control: https://docs.simplefoc.com/velocity_openloop

  // Set the open loop control type: https://docs.simplefoc.com/open_loop_motion_control
  // MotionControlType::velocity_openloop    - velocity open-loop control
  // MotionControlType::angle_openloop       - position open-loop control
  motor.controller = MotionControlType::velocity_openloop;

  // limiting voltage - [V] HDD BLDC
  // motor.voltage_limit = 2.25;  // Not too hot.
  // motor.voltage_limit = 3.0;   // Quite hot.
  // motor.voltage_limit = 3.5;   // TOO HOT.
  motor.voltage_limit = 5.0;  // CANT TOUCH THIS.

  // limiting voltage - [V] A2212/10T 1400kV 0.065 Ohm (from datasheet) --> 0.19V
  // motor.voltage_limit = 0.6;   // Ok

  // limiting current - if phase resistance provided
  motor.current_limit = 2.5;  // Amps - [A] HDD BLDC

  // The velocity open-loop control will (if not provided phase resistance),
  // set the voltage to the motor equal to the motor.voltage_limit

  // Set the torque control type: https://docs.simplefoc.com/voltage_torque_mode
  // motor phase resistance [Ohms]
  // motor.phase_resistance = 1.6; // Esta es la resistencia "phase to phase" (no es lo mismo que la "per phase"). ¿Cual tengo que poner?
  motor.phase_resistance = 0.8;  // Esta es la resistencia "phase to star point".
  // motor.torque_controller = TorqueControlType::voltage;
  // set motion control loop to be used
  // motor.controller = MotionControlType::torque;

  // Current limiting with Back-EMF compensation.
  // motor KV rating [rpm/V]
  motor.KV_rating = 1000;  // rpm/volt - HDD BLDC motor. Not measured, just guessed.
  // motor.KV_rating = 1400; // rpm/volt - A2212 T10 motor.

  // Datasheet: DRV8313 2.5-A Triple 1/2-H Bridge Driver
  // https://www.ti.com/lit/ds/symlink/drv8313.pdf
  // If the die temperature exceeds safe limits, all FETs in the H-bridge will be disabled and the nFAULT pin will be
  // driven low. Once the die temperature has fallen to a safe level operation will automatically resume. The nFAULT
  // pin will be released after operation has resumed.

  // init motor hardware
  motor.init();

  // add target command R and L.
  command.add('T', doTarget, "target velocity");
  command.add('L', doLimit, "voltage limit");

  Serial.begin(115200);
  Serial.println("Motor ready!");
  Serial.println("Set target velocity [rad/s]");
  _delay(1000);
}


// open loop velocity movement
void loop() {

  // The "rpm_limit" variable holds the final target RPM.
  // Acceleration is implemented by increasing intermediate target RPM values until "rpm_limit".

  // Compute current target RPM.
  current_rpm = angular_velocity / (2 * 3.14159265359) * 60;

  // Print current target RPM.
  elapsed = millis() - timestamp_ms;
  if (elapsed > report_interval) {
    timestamp_ms = millis();
    Serial.print("Current_RPM:");
    Serial.print(current_rpm);
  }

  // Estimate RPM from the opto-endstop trigger count.
  elapsed = millis() - counter_timestamp;
  if (elapsed > report_interval) {
    Serial.print(",Estimated_RPM:");
    Serial.println(counter / (elapsed / (1000 * 60)));  // Convert ms to minutes and calculate RPM.
    counter_timestamp = millis();
    counter = 0;
  }

  // Auto-homing at low RPM.
  // IF the interrupt has raised the state flag, and RPMs are "low" stop the motor.
  if (state) {
    // Serial.println("Endstop triggered!");
    state = LOW;  // Reset state.
    if (current_rpm <= 11) {
      rpm_limit = 0;  // HALT IMMEDIATELY
    }
  }

  // User communication.
  // This can update the value of "rpm_limit".
  command.run();

  // Adjust voltage (probably not doing anything, since it is set in the setup)
  // if(rpm_limit >= 2500 & motor.voltage_limit < 5.0){
  //   Serial.println("High voltage!");
  //   motor.voltage_limit = 5.0;
  // }
  // if (rpm_limit < 2500 & motor.voltage_limit > 2.0) {
  //   Serial.println("Low voltage!");
  //   motor.voltage_limit = 2.0;
  // }

  // Stop immediately if requested
  if (rpm_limit <= 0) {
    angular_velocity = 0;

    // Disable motor: https://community.simplefoc.com/t/motor-disable/2479/2
    if (motor.enabled == 1 & rpm_limit < 0) {
      motor.disable();
    }

  } else {
    // Enable motor: https://community.simplefoc.com/t/motor-disable/2479/2
    if (motor.enabled == 0 & rpm_limit >= 0) {
      motor.enable();
    }
  }

  // Compute new velocity limit
  angular_velocity_limit = (rpm_limit / 60) * (2 * 3.14159265359);

  // limit velocity by inverting acceleration
  // if( abs(angular_velocity) > angular_velocity_limit ){
  //   ramp_increment = -ramp_increment;
  // }
  if (angular_velocity > angular_velocity_limit) {
    // Accelerate
    ramp_increment = -abs(ramp_increment);
  } else {
    // Slow down
    ramp_increment = abs(ramp_increment);
  }

  // Motor Aceleration.
  // Increase/decrease velocity a bit, every now and then.
  if (_micros() - timestamp_us > ramp_increment_interval) {
    timestamp_us = _micros();
    angular_velocity += ramp_increment;
  }

  // using motor.voltage_limit and motor.velocity_limit
  motor.move(angular_velocity);
}
