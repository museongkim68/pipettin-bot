# HX711: MicroPython script for the Pico offset configuration through serial

The script turns on/off the built-in LED if the signal (-65000 at startup) is greater than the offset (0 at startup). The offset is set to the current signal value by sending "%" through the serial interface / stdin of the Pico.

- `%` updates the offset,
- `q` ends the loop.
- "newline" characters / "Enter keys" separate messages.

Full instructions:

1. Install MicroPython on the Pico: https://www.raspberrypi.com/documentation/microcontrollers/micropython.html

2. Upload the "sketch" to the Pico. Example using "ampy": https://mikeesto.medium.com/uploading-to-the-raspberry-pi-pico-without-thonny-53de1a10da30

```bash
pip3 install adafruit-ampy
ampy --port /dev/serial/by-id/usb-MicroPython_Board_in_FS_mode_e6605838833db138-if00 put main.py
```

3. Connect to the serial port (e.g. using `minicom`):
    - You may need to press "Ctrl+D" to trigger a soft-restart.
    - `ampy` won't send input to stdin, but `minicom` does.
    - Messages can also be sent using `echo -ne "yourmessagehere\n" > /dev/ttyACM0`.
    - Press "Ctrl+A+X" and Enter to exit `minicom` at any time.

```bash
minicom -b 115200 -D /dev/serial/by-id/usb-MicroPython_Board_in_FS_mode_e6605838833db138-if00
```

You can also send commands using `echo` and read using `cat`:

```bash
# https://unix.stackexchange.com/a/117064/460609
cat -v < /dev/serial/by-id/usb-MicroPython_Board_in_FS_mode_e6605838833db138-if00 &
```

# 80 Hz sampling hack

See: https://forum.arduino.cc/t/hx711-soldering-for-80-hz/630699

1. Add some sodder to the 15 th pin (counting counter clock-wise from the top left pin, marked with a dot).
2. Carefully lift it up with a thin soldering tip, and make sure that it is completely disconnected from the PCB trace below.
3. Bridge it to VCC using (red) wire.
4. Be happy, this is now at leasr usable for probing.

![hack.jpg](./images/hack.jpg)

