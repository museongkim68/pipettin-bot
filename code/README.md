# Code

Software for the pipettin-bot project.

For the sake of modularity, many components have been included as git submodules.

General code flow:

![code_flow.svg](./code_flow.svg)

### Protocol writer: [`pipettin-gui`](./pipettin-gui)

Source code for the Pipwttin Weiter web-app, a Node.js graphical user interface (GUI) written in the ReactJS framework.

The UI is included as a git submodule of this repo: https://gitlab.com/pipettin-bot/pipettin-gui

The original GUI, as designed and written in pure JS by Facundo, is available in the [archive branch](https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master-backup-models).

### Control

Software modules responsible for controlling hardware modules.

#### Main controller module: [`piper`](./klipper/piper)

Source files for the main python module, responsible for:

- Parsing "actions" of "mid-level" protocols from the GUI into GCODE (with a few "GCODE extensions").
- Managing connections and communications with other parts of the software stack.
    - Communicates with the GUI through a Socket.io client (e.g. receives events).
    - Operates the hardware, by sending GCODE to Klipper through Moonraker.
- Plugin system: python modules in the `plugins` directory can be loaded to add functionality to the `piper` module.

#### CNC firmware: [`klipper-setup`](./klipper-setup)

The project has moved from GRBL to Klipper, but will stick to the Arduino UNO + CNC shield. Klipper is compatible with many micro-controllers (a.k.a. MCUs), so using more expensive boards is trivial with Klipper.

The directory contains general information relevant to the specific configuration for this project.

The full Klipper software stack has been forked, included as a submodule, and is available over here:

- Meta-repo including all forks as submodules: https://gitlab.com/pipettin-bot/forks/klipper-stack
- All forks, including the Klipper software stack: https://gitlab.com/pipettin-bot/forks/

#### Hardware module firmware: [firmware](./firmware)

> Work in progress. Status: not functional.

Firmware and templates for additional hardware modules.

Python class templates for developing of new modules, and adding plugins to `piper`.

### Object generation and validation

Generation of data objects (mostly JSON) for the pipettin-bot project.

#### [`newt`](./newt)

> Work in progress. Status: partial functionality.

A python module to generate and validate JSON data objects for workspaces, platforms, contents, and protocol steps.

The idea is to make JSON "schemas" for each kind of data, and provide "primitive" methods to program the robot.

See:

- https://gitlab.com/pipettin-bot/pipettin-bot/-/issues/119
- https://gitlab.com/pipettin-bot/pipettin-bot/-/issues/139

#### [`mix`](./mix)

> Work in progress. Status: basic functionality.

Generates information to prepare solutions with fixed and variable components (i.e. different conditions), using hierarchical clustering.

It's purpose is to automate the generation of "shorter" preparation of solution variants (e.g. PCRs using one master mix, but several templates, or primers).

It needs `newt` to generate "high-level" protocol steps which can be fed to the GUI.

### Startup scripts: [`systemd.units`](./systemd.units)

`systemd.units` files to start/restart the:

- Node.js server.
- Robot driver module.
- Klipper software stack.
- Jupyter Lab server.
- GitBuilding documentation server.

Units for the Klipper stack are available at the meta repo: https://gitlab.com/pipettin-bot/forks/klipper-stack

> Some peopple hate systemd, but I don't know enough Linux to realize why. :B
