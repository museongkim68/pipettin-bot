All our documentation has moved to our wiki:

- Browse it online here: [https://docs.openlabautomata.xyz/](https://docs.openlabautomata.xyz/)
- Or directly on GitLab: [https://gitlab.com/pipettin-bot/wiki-js/-/blob/master/home.md](https://gitlab.com/pipettin-bot/wiki-js/-/blob/master/home.md)

Guides at a glance:

- **Development**: https://docs.openlabautomata.xyz/en/Development
- **Assembly**: https://docs.openlabautomata.xyz/en/Assembly-Guide
- **Installation**: https://docs.openlabautomata.xyz/en/Software-Installation
- **Usage**: https://docs.openlabautomata.xyz/en/Usage-Guide/User-Guide
- **Calibration**: https://docs.openlabautomata.xyz/en/Calibration
- **Maintenance**: https://docs.openlabautomata.xyz/en/Maintenance

More details about each component may be found in this repo, as described in the [SITEMAP.md](./SITEMAP.md).



<!--

Table of contents:

[[_TOC_]]

# Development

## Overview

## Software architecture

## Software modules

### GUI

### protocol-builder

JS library that interprets the user input, and generates protocol.json files.

### workspace-builder

JS library that interprets the user input, and generates workspace.json files.

### piper

## Arduino Firmware

## Hardware

### Electronics

### Structure and Mechanics

# Assembly guide

# Software installation

## Raspberry Pi

## Arduino firmware

# User guide

## Web GUI guide

## Machine calibration

### Tool calibration

### Workspace and platforms

### Editing platforms for multi-tool support

### Pipette volumetric calibration

-->
