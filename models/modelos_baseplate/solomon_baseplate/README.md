# Solomon's Baseplate

These parts have been designed to hold objects on the workspace in place, when they might move when used by the robot.

* Exported models:
    * [stl_models.zip](./stl_models.zip)
    * [step_models.zip](./step_models.zip)
* Sources: [solidworks_models.zip](./solidworks_models.zip)
    * In Solidworks unfortunately, and are mostly useless without the proprietary editor. Some info on how to convert these files can be found in the internet, for example [here](https://all3dp.com/2/sldprt-to-stl-how-to-convert-sldprt-files-to-stl/).

Specification for the springs:

* 15 mm long.
* 5 mm outer diameter.
* 0.8 mm wire diameter.

![preview.png](./images/preview.png)

The pegs at the bottom of the larger piece will fit into two of the holes drilled into the baseplate.

![to_print.png](./images/to_print.png)

The larger part relies on bridges for 3D printing.