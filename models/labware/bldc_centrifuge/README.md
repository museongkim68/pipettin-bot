# Swing bucket rotor for HDD motor and 1.5 mL lab tubes

> Experimental!

License details: [LICENSES.md](../../../LICENSES.md) (CERN OHL-S v2).

Documentation at: https://docs.openlabautomata.xyz/en/Mini-Docs/labware/Centrifuge

Models in [hdd_motor.FCStd](./hdd_motor.FCStd):

- `rotor.rev3`: for HHD motor "A"
- `rotor.rev3.motorB`: for HHD motor "B".
![pic.png](./images/pic.png)

Tupperfuge / Taperfuga overview:

![tupperfuga.svg](./images/tupperfuga.svg)
# Robofuge

## Variant 1: hanging rotor with springs

- Floating springs: to dampen unbalanced loads (and pave the way for self-balancing with beads).
- Socket for a round magnet and holes for the pins of a hall sensor (to support homing and RPM check).
- Inverted: because adding the hall sensor is simpler in this way, otherwise it would probably get in the way of the tubes.

![hanging_rotor1.png](./images/hanging_rotor1.png)

## Variant 2: closed-end rotor

- Separate "adapter" for the BLDC motor, coupled to the rotor through 3 mm holes.
- Empty slots to insert M3 screws for swings.
    - Could be filled by using prisoner screws instead of regular M3 screws "with heads".
- Dimensions are not final, must scale to fit the tube's full length.
![proto2.png](./images/proto2.png)
