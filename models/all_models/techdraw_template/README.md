Follow this guide faithfully and thoroughly: https://wiki.freecad.org/TechDraw_TemplateHowTo#Create_editable_fields

XML attributes must be added to the editable text fields and to the SVG document.

There are issues with two XML attributes: "transform" and "xml:space". These must be removed.

Use the "Plain SVG" export option in Inkscape.

Discord discussion: https://discord.com/channels/1042462799772782613/1111005573123670196/threads/1116050093313892392
