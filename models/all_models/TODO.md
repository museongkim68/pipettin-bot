# TO-DO

- Make baseplate assembly using [baseplate-base_parts.FCStd](./baseplate-base_parts.FCStd).
- Make toolchanger assembly.
- Make full assembly.
- Invert Z axis, and invert Z rod attachment.
    - Add mounting holes for [GT2 rack fidgets](./images/GT2-belt_lock-with_base.png) (?)


Check general issues at https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues

And documentation issues at https://gitlab.com/pipettin-bot/pipettin-grbl-docs/-/issues

# General TODO

## Reconsider total Z clearance


The original robot (non inverted) had a low point of about 200-250 mm (considering the dangling nema17 or not). The tools could be lifted from there by 180 mm (approx.).

In thenew design, with a placed tip, there will only be 6 cm of vertical clearance (11.6 cm minus 5 cm from a p200 tip).

So lame… to match the old design there should be at least 20 cm of vertical clearance with a tip placed.

![ranges.png](./images/notes/ranges.png)

Alternative: [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-shorter.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-shorter.FCStd)

![cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-shorter.png](./images/cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-shorter.png)

With that design I get 10 cm of free space. Still lame…

Third alternative, put the Z stepper in the free space of the Z axis bases, the THSL thread pointing up, and transmit the force by attaching the nut to the top ends of the Z rods.

Model: [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd)

![compact.png](./images/notes/compact.png)

# DONE

## DONE: align Z screw with Z rods

- [ ] Now that the space between the pair of Z-bases is wider, I can align the Z axis THSL nut with the rods.

![center_z_nut.png](./images/notes/center_z_nut.png)



## DONE: load cell Z-carriage

Two 5 Kg load cells: https://articulo.mercadolibre.com.ar/MLA-727250869-celda-de-carga-5kg-con-amplificador-hx711-arduino-nubbeo-_JM

Model: [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.FCStd)

I'll make a parameters spreadsheet for this, it seems that humanity has not agreed on the dimensions of 5 Kg load cells.

Likely supplier: https://articulo.mercadolibre.com.ar/MLA-727250869-celda-de-carga-5kg-con-amplificador-hx711-arduino-nubbeo-_JM

![load_cells.png](./images/load_cells.png)

The load cells must carry all weight (i.e. there must be no other support points for the "load").

Options:

1. Put the load cell on the z-carriage.
    - Issue: the load cells are too long, and not thin enough: 

![load_cell_wide.png](./images/notes/load_cell_wide.png)

2. Put the load-cell under the stepper motor.
    - More likely, fewer modifications needed.
    - Simplifies Z carriage a bit.
    - Had considered this for something else I think.

![load_cell_z_stepper.png](./images/notes/load_cell_z_stepper.png)

3. Use them as joints for the 2020 profiles.
    - Unlikely, would need 4 load cells.
    - Has the advantage of very rigid mounting points (metal-metal).

