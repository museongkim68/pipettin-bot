# 3D Models directory

Find herein 3D all printable models to make our CNC frame and exchangeable tool adapter for the pipetting robot.

Hardware is distributed under the terms of the [CERN-OHL-S](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSES.md#hardware) licence.

Most models have been migrated to a unified directory over here: [all_models](./all_models)

![assembly.png](./all_models/images/assembly.png)


Summarized contents:

- XYZ and Tool axes.
    - A generic 3-axis XYZ/cartesian CNC frame.
    - Exchangable pipetting "tools".
- Structural frame.
    - Standard 2020 aluminium extrusion.
    - Note: these parts must be cut and machined, and are not meant for 3D-printing.
- 3D-printable slider nuts and couplings for 2020-profiles
    - Models for coupling 2020 and 3030 aluminium profiles, and extra pieces for leveling.
- Cable management.
    - Cable carriers, originally in OpenSCAD format, re-modelled in FreeCAD.
    - These are adapted from original OpenSCAD designs posted at Thingiverse by [Gasolin](https://www.thingiverse.com/thing:4375470).
    - Note: these parts tolerate a small amount of "elephants foot" aberration in 3D-printers, but will print better with a well calibrated first layer, or with appropriate corrections.
- Tools.
    - Design files for "tools" that the robot can use to carry out pipetting protocols.
    - Pipette tool adapter
        - Models to mount and actuate p20, p200, or p1000 Gilson-like micropipettes.
        - The tip ejector is actuated by moving the tool under a "tip ejection post", and presing the button against it.
    - Custom micropipette: [modelos_tools](./modelos_tools)
        - Design files for an electronic micropipette.
- Tip-ejection post.
    - Pipette tips are now ejected by a slow collision with the tip-ejection post.
- Transmission belts.
    - Rack and pulley idler models for GT2 belts. See:
- Tool-changer.
    - There are two tool-changer implementations available in this repo:
        - One based on the mini-latch (this has been tested and is reliable).
        - Another one inspired on the mecanism of the "Getit 3D" printer (in development, not tested yet).
    - A Jubilee-compatible interface is in development.
- Baseplate.
    - Models for the baseplate, where protocol objects (a.k.a. "labware") can be consistently placed in the same XYZ position, relative to the rest of the machine.
    - Baseplate tool to hold labware in place tightly: [modelos_baseplate](./modelos_baseplate)
- Electronics.
    - Cases and holders for the "electronic" parts, including:
        - End-stops mounts for 8mm and 12mm rods.
    - Other models, not currently used:
        - Cooler fan base for the Arduino UNO with CNC shield.
        - Others.
- "Extra" part models: [extra_part_models](./extra_part_models)
    - Convenient models for common components.
- Labware: [labware](./labware)
    - Dreams of a well-plate incubator and orbital shaker.

# To-do

And documentation issues at https://gitlab.com/pipettin-bot/pipettin-grbl-docs/-/issues
