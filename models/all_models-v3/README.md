# Models for OLA's Pipettin bot

More information about these design files can be found at our wiki: https://docs.openlabautomata.xyz/en/Design-Files

All files in this directory are made available under the terms of the CERN-OHL-S (strongly reciprocal, [txt](https://ohwr.org/cern_ohl_s_v2.txt), [pdf](https://ohwr.org/cern_ohl_s_v2.pdf)) license. The full license text is available in the [LICENSES.md](../../LICENSES.md) file and is also available online at https://cern-ohl.web.cern.ch/.

Copyright (C) Nicolás A. Méndez 2023
