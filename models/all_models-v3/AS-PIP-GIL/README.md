# Models for the pipette tool

Models in this directory are for the printed parts of the Gilson p200, p20 and p1000 pipette adapter and actuator.

CAD file organization:

- [AS-TP-LCH.Fcstd](./AS-TP-LCH.Fcstd): Part assembly for the tool-parking system based on a "mini-latch" mechanism, including the pieptte.
    - [TP-LCH-BAS.Fcstd](./TP-LCH-BAS.Fcstd): Tool-post base part.
    - [LCH-MINI.Fcstd](./LCH-MINI.Fcstd): Model for the Mini-latch.
- [AS-GIL-TC-LCH.Fcstd](./AS-GIL-TC-LCH.Fcstd): Toolchanger assembly.
    - [AS-GIL-TC-LCH-extras.Fcstd](./AS-GIL-TC-LCH-extras.Fcstd) (accessory parts).
- [AS-GIL.Fcstd](./AS-GIL.Fcstd): Adapter assembly.
    - [GIL-BAS.Fcstd](./GIL-BAS.Fcstd): parts for the pipette adapter.
    - [GIL-TIP-PRB.Fcstd](./GIL-TIP-PRB.Fcstd): parts for the tip probes and assemblies.

Contents:

[[_TOC_]]

----

> ![p20_tool_assembled.jpg](./images/p20_tool_assembled.jpg)
>
> Current prototype.

> ![eje_S_assembly3_nobase.png](./images/eje_S_assembly3_nobase.png)
>
> Overview of the parts.

## Floating tip probe

This system uses 3D-printed parts and an opto-endstop to place tips in the fixed-depth way (constant pressure would be the other way, but was harder to achieve).

All models are here: [tools-pipette-gilson-tip_probe.FCStd](./tools-pipette-gilson-tip_probe.FCStd)

> ![IMG_7434.JPG](./images/IMG_7434.JPG)
>
> The tip sensor is "floating", allowing tips to be detected when placed and when ejected .

>![p20_tip_probe.png](./images/p20_tip_probe.png)
>
> p20 (left) and p200 (right) pipette tip probes.

> ![](images/02-tip_probe.png)
>
> First prototype probe, for the p200 pipette.

## Toolpost for tool-changing

Part assembly for the tool-parking system.

![toolpost_assembly.png](./images/toolpost_assembly.png)

# To-Do

- [ ] Consider adding Pipette Jockey's Dragon Lab pipette adapter!
    - https://youtu.be/BhQub5Xh_8o?t=1668
