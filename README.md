![pipettin.png](./doc/media/logos/pipettin.png)

# Pipetting bot

> Hackable pipetting _robotware_ for laboratory protocol automation.

Hello! Welcome to the GitLab repository of the Pipetting-bot project, part of Open Lab Automata.

We are developing what we believe to be the most affordable and open-source pipetting robot project, capable of changing pipettes, driven by a friendly web app. Most importantly, it aims for _really **awesome documentation**_.

**Contents**: This repository contains hardware design files, software, and development documentation for a completely functional prototype of our open-hardware pipetting robot and its software. It is currently a DIY project.

[[_TOC_]]

**About us**: We are a team of researchers and developers based in Buenos Aires, Argentina. If you want to chat with us, scroll to the contact information below.

**Audience** (you): This project is currently aimed at people with experience in research, hardware and/or software, that wish to build a robot and collaborate with its development (to learn how, visit: [contributing](https://gitlab.com/pipettin-bot/pipettin-grbl#contributing)). If you want this project to happen, and want to provide other kinds of support, please [contact us](https://gitlab.com/pipettin-bot/pipettin-grbl#contact-us).

## [Demo video](https://www.youtube.com/watch?v=W3rAxg5W7Jk)

An overview presentation of the robot protocol is up on YouTube: https://www.youtube.com/watch?v=W3rAxg5W7Jk

It was prepared for the final demo of the Open Hardware Makers program.

Extra eye candy:

![beta_x.jpg](./doc/media/images/beta_x.jpg)

> Objects are aligned using a simple system of 3D-printed stops or "curbs", placed on the regular grid of holes of the baseplate (which is removable, and self-aligns to the frame).
> Two tools are parked on the right, the tool carraige is on the left.
> The electronics are housed in an acrylic box, and fixed to the side of the aluminium frame (bottom right).

![old_gui_screenshot.png](./doc/media/images/old_gui_screenshot.png)

> There is an awesome web-application to program and operate the robot from your browser.

# Licensing

> 🏛️

See the [`LICENSES.md`](LICENSES.md) file for the licence terms of each part of this project.

Hardware is licenced under the [CERN-OHL-S](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSES.md#hardware) licence, software under the [GNU Affero General Public License v3.0](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSES.md#software), and the wiki's documentation under the [CC-BY-SA 4.0 licence](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSES.md#documents).

# [Contents](./SITEMAP.md)

> 📑

Get your bearings by visiting the [SITEMAP.md](./SITEMAP.md). In summary, this repo contains:

- **Sources**:
    - Software: [`code`](./code)
    - Hardware design: [`models`](./models)
- Development, Assembly, set-up, and usage instructions:
    - Latest: https://docs.openlabautomata.xyz/
    - Old docs (legacy alpha version): https://pipettin-bot.gitlab.io/pipettin-bot-docs/

> Pardon our frequent _castellano_ :)

# [Docs and Guides](./GUIDES.md)
> 📜

Links to guides and documentation are available at: [GUIDES.md](./GUIDES.md)

# [Development](./GUIDES.md)

> 🚧

Development information is contained in each folder's README file in this repo, describing their contents. They are linked to when relevant, and help navigate the repo on GitLab.

A development guide is also available at: [GUIDES.md](./GUIDES.md)

## Downloads

This repo tracks all the required repositories of pipettin as git "submodules", and uses [git LFS](https://git-lfs.com/) for tracking CAD files, models, and other large files. You may need to [install git lfs](https://docs.github.com/en/repositories/working-with-files/managing-large-files/installing-git-large-file-storage) on your system, and for convenience.

If you are using SSH to pull or push changes with an encrypted SSH private key, the [use the ssh agent](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#adding-your-ssh-key-to-the-ssh-agent) may make things simpler for you. To set it up only temporarily for a new terminal, run the following.

```bash
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa	# Adjust to match your key's path.
```

You can download all files using `git`, with the following commands.

```bash
# To download only the model files, run a bare git clone.
# git clone https://gitlab.com/pipettin-bot/pipettin-bot.git

# Download the repo and its submodules. Note that this leaves submodules in a detached state.
git clone --recurse-submodules https://gitlab.com/pipettin-bot/pipettin-bot.git

# Enter the repo's directory.
cd pipettin-bot

# Activate git LFS, and download the CAD files.
git lfs install
git lfs pull
```

To update to the latest and greatest version of everything:

```bash
# Optionally, suto-update submodules on every git pull.
# git config submodule.recurse true

# Pull the latest commit of the main repo.
git pull
git lfs pull

# Fetch latest submodules (optionally).
git submodule foreach --recursive git checkout -
git submodule foreach --recursive git pull
```

# [Contributing](./CONTRIBUTING.md)

> 🫂

The contrubuting guide is available here: [CONTRIBUTING.md](./CONTRIBUTING.md)

We will welcome collaborations on any front of the project <3

# About this project

> 💡

We set out to design and share a liquid handling platform project that anyone can use, build and modify.

And we've come far!

* Users can generate workspaces and protocols using a web-based user interface.
* Developers may study and modify the project to suit their needs, or reuse parts of it in their projects.
* It costs 250-500 USD (depending on the country) if you can supply the pipettes.

The project has four important aspects:

* Design files for a 4-axis CNC machine and tool adatpers for Gilson micropipettes.
* A web application to help users setup workspaces and protocols.
* A Python program that operates the machine.
* Growing documentation.

## Motivation

> 🙌🏼

We believe our project will fill a gap in OSH liquid handling robots. By fully complying with OSH definitions, we hope to enable anyone who wishes to independently study, build, or modify the hardware.

The acquisition cost of most pipetting robots (of at least 10.000 US dollars) is still prohibitive for all of our region’s laboratories but the elite.

We want to make a robot anyone can actually make or purchase; enabling **more people** to do **more research** with their time.

## Our objectives

> 🎯

To make a liquid handling robot which is:

- easy to use,
- fully open source,
- highly documented, 
- modular in design,
- minimal in cost,
- very hackable,
- marketable,
- and integrates well with other open labware projects.

To these ends, we are working on:

* Multimedia documentation covering most aspects of the project,
* consolidating reliance on widely available components, 3D printing, and modularization;
* using cheaper parts and make use of existing equipment (i.e. the micropipettes),
* modularizing hardware and software, implement a toolchanging system, and interface with other OScH products (thermocyclers, turbidimeters, colony-pickers, etc.).

## Project status and Roadmap

> 🚥 We've reached "alpha" status!

Learn more about the roamap and how to contribute over in [CONTRIBUTING.md](./CONTRIBUTING.md)

Our bot can:

* Be programmed using a very nice web UI.
* Place tips, load and dispense liquid solutions, and discard tips.
* Change pipettes (tools).
* Prepare a workspace for rather complex PCR mixes with a reasonably efficient automatic planner.
* Be assembled and used by anyone who reads the docs.

## Why another bot?

> 🤖❤🧪

During 2020 many pipetting robots were developed (including this one).

We believe it is a marvelous time to make pipetting robots, and there are many advanced open-source lab automation projects out there. A list of the ones I've spotted in the wild follows (note that they are not necesarily open-source):

* OTTO
* FrescoM
* McQ
* SideKick
* OT2 (not open source).
* ...

In short: other projects might be _open_, but without _free_ documentation they simply fall short. They also tend to be expensive, or are comercially unavailable.

The main justification of our initiative is to make a robot _we_ can actually buy or make, considering the local context. By that I mean that 10000 USD for an OT2 is still unattainable, and importing an OT2 to Argentina is a nightmare.

There are many advanced open-source lab automation projects out there. Each of them must have had a different reason to start over.

However, even though they are open source, their documentation is far from thorough. You might start [here](https://blog.opentrons.com/resources/), or at their [repo](https://github.com/Opentrons), and get nowhere near build instructions, or even a bill of materials (there is one available for the OT1, but that's about it). There is always the possibility that I have not found the good docs.

Insufficient documentation means that making one of those robots independently is _hard_.

You can have a look at Pipette Jockey's [blog post](http://pipettejockey.com/2018/01/03/making-a-opentrons-compatible-liquid-handling-robot/) on making an OT2 compatible bot, and his [1 hour video](https://www.youtube.com/watch?v=BhQub5Xh_8o&feature=emb_title) on the subject. I must say that he had all of those problems even with OT's help. From his video and my own experience, I gather that making one would not have saved me time, nor money (at the time I spent 300 USD on the prototype).

## What is this project not (yet)?

> ❔

These are aspects of the project that are not yet optimal:

- This project is **not** distributed as a "ready-to-use" product _yet_ (11/2022). As such, it can be considered a DIY project. We hope to have a commercial version by early 2024.
- It is not quick to build: the project can be built from scratch in one or two weeks, depending on the availablility of components and your experience in making stuff.
- It is not interoperable (yet) with other lab-automation projects.
- It does not (yet) include pipettes, but they will be ready soon (est. 2024).

# Contact us

> 💌

You can find us at:

- The GOSH forum ([thread](https://forum.openhardware.science/t/pipetting-bot-project-presentation/3797), [direct message](https://forum.openhardware.science/u/naikymen/)),
- on Gitlab (create an [issue here](https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4685246)),
- and on OLA's [discord server](https://discord.gg/QkRQp2b5Mr).

Please contact us if you are interested in the project!

# Credits and acknowledgements

> 🧑🏻‍🤝‍🧑🏾🧑‍🤝‍🧑🧑🏿‍🤝‍🧑🏽

Original development by Nicolás Méndez and Facundo Méndez:

* The original web UI and its design.
* Models for the the CNC frame and pipette actuators.
* CNC and pipette driver modules for the bot.

Collaborators:

* Fiqus coop.
* TECSCI S.A.S.
* Graduate sudents from UNSAM.
* Pipe, who designed an alternate version of the toolchanger, based on the GetIt printer.
* Solomon has worked on the baseplate system, and designed a nice holder for labware.

We are very grateful to:

* The developers of GRBL, the greatest firmware for the Arduino UNO + CNC shield.
* The community behind Klipper: "a 3D-printer firmware".
* The Jubilee3D folks, for the welcoming and supportive chats.
* The [reGOSH](https://regosh.libres.cc/en/home-en/) free tech, latin-american network, and the truly awesome people at [GOSH](https://openhardware.science/).
* The greater open source community!

We thank the Gathering for Open Science Hardware (http://openhardware.science) and the Alfred P. Sloan Foundation (https://sloan.org) for their support.

![gosh_logo.png](./doc/media/logos/gosh_logo.png "gosh_logo.png")

We thank [reGOSH](https://regosh.libres.cc/) for bringing us together, making the project possible.

![regosh_logo.png](./doc/media/logos/regosh_logo.png "regosh_logo.png")
